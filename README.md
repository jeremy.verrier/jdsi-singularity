# jdsi-singularity, mon container applicatif en 30minutes 

Ce mini tutoriel a pour objectif de vous faire mettre le pied à l'étrier sur la technologie de containerisation qu'est Singularity.

Très utilisé par le monde de la recherche car HPC (Calcul Haute Perfomance) compliant, vous pourrez aiguiller vos agents locaux sur l'utilisation de cet outil.


## Ressources utilisés pour ce TP
https://souchal.pages.in2p3.fr/hugo-perso/2018/09/18/tp-how-to-build-a-singularity-container/

https://www.hpcadvisorycouncil.com/events/2017/stanford-workshop/pdf/GMKurtzer_Singularity_Keynote_Tuesday_02072017.pdf#43 

https://hsf-training.github.io/hsf-training-singularity-webpage/aio/index.html

https://www.sdsc.edu/education_and_training/tutorials1/singularity_old.html